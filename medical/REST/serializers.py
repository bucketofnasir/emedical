from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from medical.models import hospital, has_test, package, ratings, feedback


class HospitalListSerializer(ModelSerializer):
    tests = serializers.StringRelatedField(many=True)
    packages = serializers.StringRelatedField(many=True)

    class Meta:
        model = hospital
        fields = [
            'name',
            'type',
            'tests',
            'packages',
            'email',
            'phone',
            'division'
        ]


class HospitalDetailsSerializer(ModelSerializer):
    class Meta:
        model = hospital
        fields = [
            'id',
            'name',
            'type',
            'email',
            'phone',
            'address',
            'zip_code',
            'division',
        ]


class HospitalCreateSerializer(ModelSerializer):
    class Meta:
        model = hospital
        fields = '__all__'


# Diagnostic test serializers
class TestListSerializer(ModelSerializer):
    hospital = serializers.StringRelatedField(many=False)
    name = serializers.StringRelatedField(many=False)

    class Meta:
        model = has_test
        fields = [
            'id',
            'name',
            'hospital',
            'test_details',
            'price',
            'delivary_in',
            'is_home_deliverable'
        ]


class TestCreateSerializer(ModelSerializer):
    class Meta:
        model = has_test
        fields = [
            'name',
            'hospital',
            'test_details',
            'price',
            'delivary_in',
            'is_home_deliverable'
        ]


class TestReadSerializer(ModelSerializer):
    class Meta:
        model = has_test
        fields = [
            'pk',
            'name',
            'hospital',
            'test_details',
            'price',
            'delivary_in',
            'is_home_deliverable'
        ]


# Package Serializer
class PackageListSerializer(ModelSerializer):

    class Meta:
        model = package
        fields = [
            'id',
            'name',
            'hospital',
            'details',
            'price',
            'delivary_in',
            'remarks',
        ]


class PackageCreateSerializer(ModelSerializer):
    class Meta:
        model = package
        fields = '__all__'


# Ratings Serializers

class RatingListSerializer(ModelSerializer):
    class Meta:
        model = ratings
        exclude = ['id']


class RatingDetailsSerializer(ModelSerializer):
    class Meta:
        model = ratings
        fields = '__all__'
