# E-Medical #
To run the project first of all clone the project using the following command

```git clone https://bucketofnasir@bitbucket.org/bucketofnasir/emedical.git```

Now navigate to emedical directory. perform following command

```python manage.py migrate```


Create a super user using


```python manage.py createsuperuser```

Now set user credential.

Run the following command to run the project

```python manage.py runserver```
