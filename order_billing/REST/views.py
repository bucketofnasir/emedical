from django.db.models import Q
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView
)
from order_billing.models import Test_order
from .serializers import (
    OrderListSerializer,
    OrderDetailsSerializer,
)


class OrderCreateAPI(CreateAPIView):
    queryset = Test_order.objects.all()
    serializer_class = OrderDetailsSerializer


class OrderListApi(ListAPIView):
    serializer_class = OrderListSerializer

    def get_queryset(self, *args, **kwargs):
        query_set = Test_order.objects.all()
        query = self.request.GET.get('q')
        if query:
            query_set = query_set.filter(
                Q(payment_status__icontains=query) |
                Q(token_num__icontains=query) |
                Q(is_completed__icontains=query)
            ).distinct()
        return query_set


class OrderRetrieveApi(RetrieveAPIView):
    queryset = Test_order.objects.all()
    serializer_class = OrderDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class OrderUpdateApi(UpdateAPIView):
    queryset = Test_order.objects.all()
    serializer_class = OrderDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'


class OrderDeleteApi(DestroyAPIView):
    queryset = Test_order.objects.all()
    serializer_class = OrderDetailsSerializer
    lookup_field = 'pk'
    lookup_url_kwarg = 'id'
