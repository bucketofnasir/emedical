from rest_framework.serializers import ModelSerializer, StringRelatedField
from order_billing.models import Test_order


class OrderListSerializer(ModelSerializer):
    user = StringRelatedField(many=False)
    test = StringRelatedField(many=False)

    class Meta:
        model = Test_order
        fields = [
            'id',
            'user',
            'test',
            'payment_status',
            'token_num',
            'verify',
            'is_completed',
            'test_taking_date',
            'test_taking_time',
            'status'
        ]


class OrderDetailsSerializer(ModelSerializer):
    class Meta:
        model = Test_order
        fields = '__all__'
