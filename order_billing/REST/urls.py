from django.urls import path
from order_billing.REST.views import (
    OrderListApi,
    OrderRetrieveApi,
    OrderCreateAPI,
    OrderUpdateApi,
    OrderDeleteApi
)
app_name = "medical"


urlpatterns = [
    path('api/order/create', OrderCreateAPI.as_view(), name="create_order"),
    path('api/orders/', OrderListApi.as_view(), name="orders"),
    path('api/order/<int:id>', OrderRetrieveApi.as_view(), name="single_order"),
    path('api/order/update/<int:id>', OrderUpdateApi.as_view(), name="update_order"),
    path('api/order/delete/<int:id>', OrderDeleteApi.as_view(), name="delete_order"),
]

